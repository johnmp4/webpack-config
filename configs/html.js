const Config = require('../config')

class HtmlConfig extends Config {
    constructor(config) {
        super(config)
        this.dependencies.push('html-loader')
        this.loaderRule = {
            test: /\.html$/,
            loader: 'html-loader'
        }
    }

    // output() {
    //     return super.output()
    // }

    // dependenciesMet() {
    //     return Config.checkDependencies(this.dependencies)
    // }
}

module.exports = HtmlConfig
