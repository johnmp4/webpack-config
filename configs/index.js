module.exports = [
    require('./output'),
    require('./babel'),
    require('./html'),
    require('./handlebars'),
    require('./styles'),
    require('./images')
]