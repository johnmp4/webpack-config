const Config = require('../config')

class HandlebarsConfig extends Config {
    constructor(config) {
        super(config)
        this.dependencies.push('handlebars', 'handlebars-loader')
        this.loaderRule = {
            test: /\.(hbs|handlebars)$/,
            loader: 'handlebars-loader'
        }
    }
}

module.exports = HandlebarsConfig
