const Config = require('../config')

class StylesConfig extends Config {
    constructor(config) {
        super(config)
        this.dependencies.push('css-loader')
        this.loaderRule = {
            test: /\.css$/,
            use: ['css-loader']
        }
    }
    
    output() {
        this.loadOrExtract()
        this.sassLoader()
        return super.output()
    }
    
    loadOrExtract() {
        if (this.config.mode === 'development' && Config.checkDependencies('style-loader')) {
            this.loaderRule.use.unshift('style-loader')
        } else if (Config.checkDependencies('mini-css-extract-plugin')) {
            const MiniCssExtractPlugin = require('mini-css-extract-plugin')

            this.plugins.push(new MiniCssExtractPlugin({
                filename: Config.option('asset_directory') + '/[name].css?[hash]'
            }))
            this.loaderRule.use.unshift(MiniCssExtractPlugin.loader)
            
            // TODO: this.minify()
        }
    }
    
    sassLoader() {
        if (Config.checkDependencies(['sass-loader', 'node-sass'])) {
            this.loaderRule.test = /\.s?css$/
            this.loaderRule.use.push('sass-loader')
        }
    }
}

module.exports = StylesConfig
