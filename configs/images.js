const Config = require('../config')

class ImagesConfig extends Config {
    constructor(config) {
        super(config)
        this.dependencies.push('file-loader')
        this.loaderRule = {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loader: 'file-loader'
        }
    }

    output() {
        this.loaderOptions()
        this.urlLoader()
        // TODO: this.imgLoader()
        return super.output()
    }
    
    loaderOptions() {
        this.loaderRule.options = {
            context: Config.option('file_loader_context'),
            name: Config.option('asset_directory') + '/[path][name].[ext]?[hash]'
        }
    }

    urlLoader() {
        if (Config.checkDependencies('url-loader')) {
            this.loaderRule.loader = 'url-loader'
            this.loaderRule.options.limit = Config.option('data_uri_limit')
        }
    }
}

module.exports = ImagesConfig
