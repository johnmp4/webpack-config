const path = require('path')
const Config = require('../config')

class OutputConfig extends Config {
    output() {
        this.config.output = {
            filename: Config.option('asset_directory') + '/[name].js?[contenthash]',
            publicPath: '/'
        }

        if (Config.option('output_path')) {
            this.config.output.path = path.resolve(__dirname, Config.option('output_path'))
        }

        return super.output()
    }
}

module.exports = OutputConfig
