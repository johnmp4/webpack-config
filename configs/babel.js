const Config = require('../config')

class BabelConfig extends Config {
    constructor(config) {
        super(config)
        this.dependencies.push('babel-loader', '@babel/core', '@babel/preset-env')
        this.loaderRule = {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env']
            }
        }
    }

    output() {
        if (Config.checkDependencies(['@babel/plugin-transform-runtime', '@babel/runtime'])) {
            this.loaderRule.options.plugins = ['@babel/plugin-transform-runtime'];
        }

        return super.output()
    }
}

module.exports = BabelConfig
