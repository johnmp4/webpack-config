
class Message {
    constructor(msg) {
        this.msg = msg
    }

    log() {
        window.console && console.log(this.msg)
    }
}

new Message('this is a test').log()
require('./index.css')
require('./index.scss')
document.body.innerHTML = require('./index.html')
document.body.innerHTML+= require('./index.hbs')({
    image: require('./images/small.jpg')
})