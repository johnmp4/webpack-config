const merge = require('webpack-merge')
const path = require('path')
const semver = require('semver')
const project = require(path.join(process.cwd(), 'package.json'))
const wpconfig = require('./package.json')

class Config {
    constructor(config = {}) {
        this.config = config
        this.dependencies = []
        this.plugins = []
    }

    output() {
        if (this.loaderRule) this.config = Config.addLoaderRule(this.config, this.loaderRule)
        if (this.plugins) this.config = Config.addPlugins(this.config, this.plugins)
        return this.config
    }

    dependenciesMet() {
        return Config.checkDependencies(this.dependencies)
    }

    static addPlugins(config, plugins) {
        return merge.smart(config, { plugins: plugins })
    }

    static addLoaderRule(config, rule) {
        return merge.smart(config, {
            module: {
                rules: [rule]
            }
        })
    }

    static checkDependencies(required) {
        if (typeof required === 'string') required = [required]

        let met = required.filter(dep => {
            return Config.availablePackageNames.includes(dep)
                && semver.satisfies(require(path.join(dep, 'package.json')).version, wpconfig.peerDependencies[dep]);
        });
    
        return required.length === met.length;
    }

    static option(name) {
        return process.env['npm_package_config_webpack_config_' + name]
            || wpconfig.config.webpack_config[name]
    }
}

Config.merge = merge
Config.semver = semver
Config.project = project
Config.availablePackages = merge(
    project.dependencies && project.dependencies || {},
    project.devDependencies && project.devDependencies || {},
    project.optionalDependencies && project.optionalDependencies || {}
)
Config.availablePackageNames = Object.getOwnPropertyNames(Config.availablePackages)

module.exports = Config
