const configs = require('./configs')

let output = {
    mode: process.env.NODE_ENV === 'production' && 'production'
    || process.argv.includes('-p') && 'production'
    || 'development'
}

configs.forEach(config => {
    config = new config(output)
    if (config.dependenciesMet()) output = config.output()
})

module.exports = output
